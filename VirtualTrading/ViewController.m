//
//  ViewController.m
//  VirtualTrading
//
//  Created by helfy on 14-3-7.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "ViewController.h"
#import "VTHistoricalPricesViewController.h"
#import "VTAccountDetailsViewController.h"
#import "VTSettingViewController.h"
@interface ViewController ()
{
    UITabBar *tabBar;
    NSMutableArray *viewControllers;
}
@end

@implementation ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    viewControllers = [[NSMutableArray alloc] init];
    
    VTHistoricalPricesViewController *historicalPricesViewController = [[VTHistoricalPricesViewController alloc] initWithNibName:nil bundle:Nil];
    [viewControllers addObject:historicalPricesViewController];
    
    VTAccountDetailsViewController *accountViewController = [[VTAccountDetailsViewController alloc] initWithNibName:nil bundle:Nil];
    [viewControllers addObject:accountViewController];
    
    VTSettingViewController *settingViewController = [[VTSettingViewController alloc] initWithNibName:nil bundle:Nil];
    [viewControllers addObject:settingViewController];
    
    
	// Do any additional setup after loading the view, typically from a nib.
    
    tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds)-50, CGRectGetWidth(self.view.bounds), 50)];
    
    [tabBar setItems:@[[[UITabBarItem alloc] initWithTitle:@"价格走势" image:nil tag:0],
                       [[UITabBarItem alloc] initWithTitle:@"虚拟交易" image:nil tag:1],
                       [[UITabBarItem alloc] initWithTitle:@"设置" image:nil tag:2]]];
    [tabBar setSelectedItem:[tabBar.items objectAtIndex:0]];
    tabBar.delegate = (id)self;
    
    UIViewController *viewController =[viewControllers objectAtIndex:tabBar.selectedItem.tag];
    [self.view addSubview:viewController.view];
     [self.view addSubview:tabBar];
}
- (void)tabBar:(UITabBar *)_tabBar didSelectItem:(UITabBarItem *)item
{
//    int tag= item.tag;
    UIViewController *viewController =[viewControllers objectAtIndex:tabBar.selectedItem.tag];
    [self.view addSubview:viewController.view];
    
    [self.view addSubview:tabBar];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
