//
//  VTHistoryView.h
//  VirtualTrading
//
//  Created by helfy on 14-3-10.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ELineChart.h"
@interface VTHistoryView : UIView

@property (strong, nonatomic) ELineChart *eLineChart;
@property (weak, nonatomic) IBOutlet UILabel *numberTaped;
@end
