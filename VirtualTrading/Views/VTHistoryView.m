//
//  VTHistoryView.m
//  VirtualTrading
//
//  Created by helfy on 14-3-10.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "VTHistoryView.h"
@interface VTHistoryView ()
@property (strong, nonatomic) NSMutableArray *eLineChartData;

@property (nonatomic) float eLineChartScale;
@end
@implementation VTHistoryView
@synthesize eLineChart = _eLineChart;
@synthesize eLineChartData = _eLineChartData;
@synthesize numberTaped = _numberTaped;
@synthesize eLineChartScale = _eLineChartScale;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        NSMutableArray *tempArray = [NSMutableArray array];
        for (int i = 0 ; i < 1; i++)
        {
            int number = arc4random() % 100;
            ELineChartDataModel *eLineChartDataModel = [[ELineChartDataModel alloc] initWithLabel:[NSString stringWithFormat:@"%d", i] value:number index:i unit:@"kWh"];
            [tempArray addObject:eLineChartDataModel];
        }
        _eLineChartData = [NSMutableArray arrayWithArray:tempArray];
        _eLineChart = [[ELineChart alloc] initWithFrame:CGRectMake(0, 150, CGRectGetWidth(self.frame), 300)];
        [_eLineChart setDelegate:(id)self];
        [_eLineChart setDataSource:(id)self];
        [self addSubview:_eLineChart];
    }
    return self;
}

#pragma -mark- ELineChart DataSource
- (NSInteger) numberOfPointsInELineChart:(ELineChart *) eLineChart
{
    return [_eLineChartData count];
}

- (NSInteger) numberOfPointsPresentedEveryTime:(ELineChart *) eLineChart
{
    //    NSInteger num = 20 * (1.0 / _eLineChartScale);
    //    NSLog(@"%d", num);
    return 20;
}

- (ELineChartDataModel *)     highestValueELineChart:(ELineChart *) eLineChart
{
    ELineChartDataModel *maxDataModel = nil;
    float maxValue = -FLT_MIN;
    for (ELineChartDataModel *dataModel in _eLineChartData)
    {
        if (dataModel.value > maxValue)
        {
            maxValue = dataModel.value;
            maxDataModel = dataModel;
        }
    }
    return maxDataModel;
}

- (ELineChartDataModel *)     eLineChart:(ELineChart *) eLineChart
                           valueForIndex:(NSInteger)index
{
    if (index >= [_eLineChartData count] || index < 0) return nil;
    return [_eLineChartData objectAtIndex:index];
}

#pragma -mark- ELineChart Delegate

- (void)eLineChartDidReachTheEnd:(ELineChart *)eLineChart
{
    NSLog(@"Did reach the end");
}

- (void)eLineChart:(ELineChart *)eLineChart
     didTapAtPoint:(ELineChartDataModel *)eLineChartDataModel
{
    NSLog(@"%d %f", eLineChartDataModel.index, eLineChartDataModel.value);
    [_numberTaped setText:[NSString stringWithFormat:@"%.f", eLineChartDataModel.value]];
    
}

- (void)    eLineChart:(ELineChart *)eLineChart
 didHoldAndMoveToPoint:(ELineChartDataModel *)eLineChartDataModel
{
    [_numberTaped setText:[NSString stringWithFormat:@"%.f", eLineChartDataModel.value]];
}

- (void)fingerDidLeaveELineChart:(ELineChart *)eLineChart
{
    
}

- (void)eLineChart:(ELineChart *)eLineChart
    didZoomToScale:(float)scale
{
    //    _eLineChartScale = scale;
    //    [_eLineChart removeFromSuperview];
    //    _eLineChart = nil;
    //    _eLineChart = [[ELineChart alloc] initWithFrame:CGRectMake(0, 100, CGRectGetWidth(self.view.frame), 300)];
    //	[_eLineChart setDelegate:self];
    //    [_eLineChart setDataSource:self];
    //    [self.view addSubview:_eLineChart];
}

@end
