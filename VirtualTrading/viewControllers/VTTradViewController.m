//
//  VTTradViewController.m
//  VirtualTrading
//
//  Created by helfy on 14-3-7.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "VTTradViewController.h"

@interface VTTradViewController ()

@end

@implementation VTTradViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
