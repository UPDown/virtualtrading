//
//  VTBaseViewController.m
//  VirtualTrading
//
//  Created by helfy on 14-3-7.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "VTBaseViewController.h"

@interface VTBaseViewController ()

@end

@implementation VTBaseViewController
@synthesize titleLabel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
self.view.backgroundColor = [UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1];
	// Do any additional setup after loading the view.
    UIView *topBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44+(IsIOS7?20:0))];
    topBarView.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:242/255.0 alpha:1];
    [self.view addSubview:topBarView];
    
    titleLabel= [[UILabel alloc] initWithFrame:CGRectMake(0, (IsIOS7?20:0), 320, 44)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text =@"";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [topBarView addSubview:titleLabel];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 44+(IsIOS7?20:0)-1, self.view.bounds.size.width, 1.0f)];
    lineView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.8];
    [topBarView addSubview:lineView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
