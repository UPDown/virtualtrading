//
//  VTSettingViewController.m
//  VirtualTrading
//
//  Created by helfy on 14-3-7.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "VTSettingViewController.h"

@interface VTSettingViewController ()

@end

@implementation VTSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.titleLabel.text= @"设置";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
