//
//  VTAccountDetailsViewController.m
//  VirtualTrading
//
//  Created by helfy on 14-3-7.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "VTAccountDetailsViewController.h"

@interface VTAccountDetailsViewController ()

@end

@implementation VTAccountDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
        self.titleLabel.text= @"虚拟账户";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
